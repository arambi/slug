<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
      $slugs = $this->table( 'slugs');
      $slugs
            // El tipo de contenido
            ->addColumn( 'slug', 'string', ['limit' => 255])
            ->addColumn( 'model', 'string', ['limit' => 16])
            ->addColumn( 'foreign_key', 'integer', ['null' => false, 'default' => 0, 'limit' => 8])
            ->addColumn( 'locale', 'string', ['limit' => 5])
            ->addColumn( 'created', 'datetime', ['default' => null])
            ->addColumn( 'modified', 'datetime', ['default' => null])
            ->addIndex( ['model'])
            ->addIndex( ['foreign_key'])
            ->addIndex( ['locale'])
            ->addIndex( ['slug'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'slugs');
    }
}