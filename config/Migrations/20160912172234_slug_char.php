<?php

use Phinx\Migration\AbstractMigration;

class SlugChar extends AbstractMigration
{

  public function change()
  {
    $slugs = $this->table( 'slugs');
    $slugs
          // El tipo de contenido
          ->changeColumn( 'model', 'string', ['limit' => 32])
          ->save();
  }
}
