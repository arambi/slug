<?php
use Cake\Routing\Router;

Router::plugin('Slug', function ($routes) {
    $routes->fallbacks();
});
