<?php
namespace Slug\Model\Behavior;

use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Inflector;
use Cake\I18n\I18n;
use Slug\Model\Entity\Slug;
use \ArrayObject;
use I18n\Lib\Lang;
use Cake\Event\EventManager;
use Cake\Event\EventManagerTrait;
use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Log\Log;

/**
 * Sluggable behavior
 */
class SluggableBehavior extends Behavior 
{
  use EventManagerTrait;

/**
 * Configuración del Behavior
 * @var type 
 */
  protected $_defaultConfig = [
      'field' => 'title',
      'alias' => false,
      'implementedFinders' => [
          'slug' => 'findBySlug',
      ]
  ];

  public function initialize( array $config) 
  {
    $this->_setupFieldAssociation();

    // Añade el campo a CrudConfig
    $this->_table->crud->addFields([
      'slugs' => [
        'label' => __d( 'admin', 'Enlace'),
        'type' => 'hasMany',
        'template' => 'Slug.fields.slugs',
        'help' => __d( 'admin', 'Es el nombre del enlace para este contenido. Será siempre en minúsculas, sin espacios y sin caracteres especiales excepto guión (-).')
      ],
    ]);


  }


  public function implementedEvents()
  {
    $events = parent::implementedEvents();
    $events ['Crud.afterSetEmptyProperties'] = 'afterSetEmptyProperties';
    return $events;
  }

  public function afterSetEmptyProperties( Event $event, ArrayObject $data)
  {
    $langs = Lang::iso3();
    foreach( $langs as $iso3 => $lang)
    {
      $exists = false;
      
      if( is_array( $data ['slugs']))
      {
        foreach( (array)$data ['slugs'] as $slug)
        {
          if( $slug ['locale'] == $iso3)
          {
            $exists = true;
            break;
          }
        }
      }

      if( !$exists)
      {
        if( empty( $data ['slugs']))
        {
          $data ['slugs'] = [];
        }
        
        $data ['slugs'][] = [
          'locale' => $iso3,
          'model' => $this->_table->getAlias(),
          'slug' => ''
        ];
      }
    }
  }

  /**
 * 
 * @param Event $event
 * @param Entity $entity
 */
  public function beforeSave( Event $event, Entity $entity) 
  {
    $this->prepareFields( $entity);
  }

/**
 * Añade la asociación Slugs
 *   
 * @param  Event       $event  
 * @param  ArrayObject $data   
 * @param  ArrayObject $options
 */
  public function beforeMarshal( Event $event, ArrayObject $data, ArrayObject $options)
  {
    // Añade
    $options ['associated'][] = 'Slugs';
  }

/**
 * Añade la asociación Slugs 
 * 
 * @param  Event  $event   
 * @param  Query  $query   
 * @param  [type] $options 
 */
  public function beforeFind( Event $event, Query $query, $options)
  {
    if( !$this->_table->isAdmin())
    {
      $query->contain( 'Slugs');
    }
    else
    {
      if( $this->_table->crud->action() != 'index')
      {
        $query->contain( 'Slugs');
      }
    }
  }

/**
 * Añade la asociación hasMany  
 */
  protected function _setupFieldAssociation()
  {

    if( !$this->getConfig( 'alias'))
    {
      $alias = $this->_table->getAlias();
    }
    else
    {
      $alias = $this->getConfig( 'alias');
    }

    $this->_table->hasMany( 'Slugs', [
          'className' => 'Slug.Slugs',
          'foreignKey' => 'foreign_key',
          'conditions' => [
            'Slugs.model' => $alias
          ],
          'propertyName' => 'slugs',
          'dependent' => true
      ]);
  }

/**
 * Prepara los campos para que sean guardados por hasMany
 * 
 * @param  Entity $entity
 */
  public function prepareFields( Entity $entity)
  {
    $field = $this->getConfig( 'field');

    if( !$entity->has( '_translations') 
      || (!$entity->has( $field) && !$this->_hasTranslation()))
    {
      return;
    }
    
    $slugs = $entity->slugs;

    if( $this->_hasTranslation())
    {
      $locales = array_keys( Lang::iso3());

      foreach( $locales as $locale)
      {
        $translation = $entity->translation( $locale);
        $slug = $entity->slugger( $locale);

        if( empty( $slug))
        {
          $data = [
            'slug' => substr($entity->get($field), 0, 250),
            'model' => $this->_table->getAlias(),
            'locale' => $locale
          ];

          $slug = new Slug($data);
          $slugs [] = $slug;
        }

        $value = !empty( $translation->$field) ? $translation->$field : $this->untilTitle( $entity, $field);
        $value = substr($value, 0, 250);
        
        if( $slug && empty( $slug->slug))
        {
          if( !empty( $value))
          {
            $slug->slug = $value;
          }
          else
          {
            return;
          }
        }

        // No tiene que haber slug pasado desde los datos
        // Es decir, si llegan datos del slug del formulario, no calculamos el slug a partir del campo
        // if( (!$slug || empty( $slug->slug)) && $translation->has( $field))
        if( (!$slug || empty( $slug->slug)) && $translation->has( $field))
        {
          if( $slug)
          {
            $slug->slug = $value;
          }
          else
          {
            $data = [
              'slug' =>  substr($value, 0, 250),
              'model' => $this->_table->getAlias(),
              'locale' => $locale
            ];
            if( isset( $slug->id))
            {
              $data ['id'] = $slug->id;
            }

            $slugs [] = new Slug( $data);
          }
        }
        elseif( !$slug || empty( $slug->slug))
        {
          $data = [
            'slug' => $value,
            'model' => $this->_table->getAlias(),
            'locale' => $locale
          ];
          if( isset( $slug->id))
          {
            $data ['id'] = $slug->id;
          }

          $slugs [] = new Slug( $data);
        }

      }
    }
    else
    {

    }

    $entity->slugs = $slugs;
  }

  private function untilTitle( $entity, $field)
  {
    $locales = array_keys( Lang::iso3());

    foreach( $locales as $locale)
    {
      if( !empty( $entity->translation( $locale)->get( $field)))
      {
        return $entity->translation( $locale)->get( $field);
      }
    }
  }

/**
 * Verifica que el campo "sluggeado" tiene traducción
 * 
 * @return boolean
 */
  private function _hasTranslation()
  {
    if( $this->_table->hasBehavior( Configure::read( 'I18n.behaviorName')))
    {
      $fields = $this->_table->translateFields();

      if( in_array( $this->config( 'field'), $fields))
      {
        return true;
      }
    }

    return false;
  }



/**
 * Devuelve las claves de las locales disponibles
 * 
 * @return array
 */
  protected function _getLocales()
  {
    return array_keys( Lang::iso3());
  }

/**
 * Asocia HasOne para Slugs con las condiciones de locale y model
 */
  public function associateSlug()
  {
    $this->_table->hasOne( 'Slug', [
      'className' => 'Slug.Slugs',
      'foreignKey' => 'foreign_key',
      'conditions' => [
        'Slug.model' => $this->_table->getAlias(),
        'Slug.locale' => I18n::getLocale()
      ],
      'propertyName' => 'slug',
    ]);
  }

/**
 * Custom finder
 * Busca el slug en la tabla slugs
 * 
 * @param  Query  $query
 * @param  array  $options
 * @return Query
 */
  public function findBySlug( Query $query, array $options)
  {
    $options = array_merge([
      'paramName' => 'slug',
    ], $options);
    
    $this->associateSlug();
    $query->where([
        'Slug.slug' => $options ['slug']
    ]);
    

    $query->contain( ['Slug', 'Slugs']);

    $query->formatResults( function( $results) use( $options){
      $links = [];
      
      return $results->map(function( $row) use( $links, $options){

        foreach( $row->slugs as $slug)
        {
          if( $slug->locale == I18n::getLocale())
          {
            continue;
          }

          if( array_key_exists( 'saveParam', $options))
          {
            Configure::write( 'I18n.params.'. $options ['saveParam'] . '.'. $slug->locale, $slug->slug);
          }
          else
          {
            $params = [
              $options['paramName'] => $slug->slug,
              'locale' => $slug->locale,
              'cat' => Router::getRequest( true)->getParam( 'cat'),
            ];

            if( array_key_exists( 'params', $options)) {
              foreach( $options['params'] as $param) {
                $params [$param] = Router::getRequest( true)->getParam($param);
              }
            } 

            $urlParams = $this->getParamVariables( Router::getRequest( true)->getParam( '_matchedRoute'));

            foreach( $urlParams as $param)
            {
              if( $res = Configure::read( 'I18n.params.'. $param . '.'. $slug->locale))
              {
                $params [$param] = $res;
              }
            }

            try {
              $url =  Router::url( $params);
            } catch (\Exception $e) {
              
            }

            if( isset( $url))
            {
              if (Configure::read('Country')) {
                $_url = substr( $url, 0, 4) . Lang::getIso2( $slug->locale) . substr( $url, 6);
              } else {
                $_url = Lang::getIso2( $slug->locale) . substr( $url, 3);
              }

              $links [Lang::getIso2( $slug->locale)] = [
                'name' => Lang::getName( $slug->locale),
                'link' => $_url
              ];
            }
          }
        }
        
        Configure::write( 'I18n.links', $links);
        return $row;
      });
    });

    return $query;
  }

  public function getParamVariables( $url)
  {
    $parts = explode( '/', $url);
    $params = [];

    foreach( $parts as $part)
    {
      if( strpos( $part, ':') !== false)
      {
        $param = str_replace( ':', '', $part);

        if( !in_array( $param, ['lang', 'slug']))
        {
          $params [] = $param;
        }
      }
    }

    return $params;
  }

}