<?php
namespace Slug\Model\Entity;

use Cake\ORM\Entity;

/**
 * Slug Entity.
 */
class Slug extends Entity
{

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      'model' => true,
      'foreign_key' => true,
      'locale' => true,
      'slug' => true
  ];

}
