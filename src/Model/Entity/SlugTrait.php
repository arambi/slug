<?php

namespace Slug\Model\Entity;

use Cake\ORM\Entity;
use Cake\Collection\Collection;
use Cake\I18n\I18n;

/**
 * Contiene utilidades y método mágicos para las entidades que tengan el Behavior Slugglable
 */
trait SlugTrait
{

/**
 * Devuelve un slug dado como parámetro un idioma (iso3)
 * 
 * @param  string $locale
 * @return string|null
 */
  public function slugger( $locale)
  {
    if( isset( $this->slugs))
    {
      $collection = new Collection( $this->slugs);
      $slug = $collection->firstMatch(['locale' => $locale]);
      return $slug;
    }

    return null;
  }

/**
 * Método mágico para la propiedad `slug`
 * Devuelve el slug para el idioma actual
 * 
 * @return string|null
 */
  protected function _getSlug()
  {
    $locale = I18n::locale();

    if( isset( $this->slugs))
    {
      if( is_object( $this->slugs))
      {
        return $this->slugs->slug;
      }
      
      $collection = new Collection( $this->slugs);
      $slug = $collection->firstMatch(['locale' => $locale]);

      if( $slug)
      {
        return $collection->firstMatch(['locale' => $locale])->slug;
      }
    }

    return null;
  }
}
