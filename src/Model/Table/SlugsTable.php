<?php
namespace Slug\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Slug\Model\Entity\Slug;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Slug\Slugify\Slugify;

/**
 * Slugs Model
 */
class SlugsTable extends Table
{

/**
 * La instancia de Slugify
 * @var Slugify
 */
  protected $_slugify;

/**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table('slugs');
    $this->displayField('id');
    $this->primaryKey('id');
    $this->addBehavior('Timestamp');
    $this->addBehavior( 'Manager.Crudable');
  }

  public function beforeSave( Event $event, Entity $entity) 
  {
    if( $entity->isNew())
    {
      $exists = $this->find()
        ->where([
          'model' => $entity->model,
          'locale' => $entity->locale,
          'foreign_key' => $entity->foreign_key
        ])
        ->first();

      if( $exists)
      {
        $entity->set( 'id', $exists->id);
        $entity->isNew( false);
      }
    }
    
    $entity->slug = $this->slug( $entity->slug);
    $this->__doSlug( $entity);
  }

  private function __doSlug( $entity, $number = 0)
  {
    $slug = substr($entity->slug, 0, 240);

    if( empty( $slug))
    {
      return;
    }
    
    if( $number > 0)
    {
      $slug .= '-'. $number;
    }

    if( !$this->__exists( $entity, $slug))
    {
      $entity->slug = $slug;
      return;
    }

    $number++;
    $this->__doSlug( $entity, $number);
  }

  private function __exists( $entity, $slug)
  {
    $conditions = [
      'model' => $entity->model,
      'locale' => $entity->locale,
      'slug' => $slug,
    ];

    if( !$entity->isNew())
    {
      $conditions ['NOT'] = [
        'id' => $entity->id
      ];
    }

    return $this->exists( $conditions);
  }

/**
 * 
 * @param Entity $entity
 */
  public function slug( $title) 
  {
    return $this->slugify()->slugify( $title, '-');
  }

/**
 * Devuelve la instancia de Slugify
 * 
 * @return Slugify
 */
  public function slugify()
  {
    if( empty( $this->_slugify))
    {
      $this->_slugify = new Slugify();
    }

    return $this->_slugify;
  }
}
