<?php

namespace Slug\Shell;

use I18n\Lib\Lang;
use Cake\Console\Shell;
use Slug\Model\Entity\Slug;

/**
 * Reslug shell command.
 */
class ReslugShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $model = $this->in('Indica un model (plugin.model)');
        $this->Model = $this->getTableLocator()->get($model);
        $SlugsTable = $this->getTableLocator()->get('Slug.Slugs');
        $contents = $this->Model->find('translations');

        foreach ($contents as $content) {
            $langs = collection(Lang::get())->extract('iso3')->toArray();
            foreach ($langs as $lang) {
                $title = $content->translation($lang)->get($this->Model->getDisplayField());
                $has = false;

                foreach ($content->slugs as $slug) {
                    if ($slug->locale == $lang) {
                        $slug->set('slug', $title);
                        $has = true;
                    }
                }

                if (!$has) {
                    $slug = $SlugsTable->newEntity([
                        'slug' => $title,
                        'model' => $this->Model->getAlias(),
                        'locale' => $lang,
                        'foreign_key' => $content->id,
                    ]);

                    $SlugsTable->save($slug);

                    $content->slugs[] = $slug;
                }
            }
            
            $this->Model->save($content);
        }
    }
}
