<div class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <div ng-class="{'input-group': data.languages.length > 1}">
      <set-languages></set-languages>
      <input ng-class="{ hide: data.currentLanguage.iso3 != slug.locale }" ng-repeat="slug in content[field.key]" ng-model="slug.slug" type="text" class="form-control">
      <span class="form-error" ng-if="field.error" ng-repeat="error in field.error">{{ error }}</span>
    </div>
  </div>
</div>

