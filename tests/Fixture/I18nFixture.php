<?php
namespace Slug\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class I18nFixture extends TestFixture {

/**
 * table property
 *
 * @var string
 */
  public $table = 'i18n';

/**
 * fields property
 *
 * @var array
 */
  public $fields = array(
    'id' => ['type' => 'integer'],
    'locale' => ['type' => 'string', 'length' => 6, 'null' => false],
    'model' => ['type' => 'string', 'null' => false],
    'foreign_key' => ['type' => 'integer', 'null' => false],
    'field' => ['type' => 'string', 'null' => false],
    'content' => ['type' => 'text'],
    '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]],

  );

/**
 * records property
 *
 * @var array
 */
  public $records = array(
    array('locale' => 'spa', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Hola mundo'),
    array('locale' => 'eng', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Hello world'),

  );
}