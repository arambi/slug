<?php
namespace Slug\Test\TestCase\Model\Behavior;

use Cake\Datasource\ConnectionManager;
use Slug\Model\Behavior\SluggableBehavior;
use Cake\TestSuite\TestCase;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\I18n\I18n;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;
use Cake\ORM\Entity;
use Cake\Core\Plugin;

class ContentsTable extends Table 
{

  public function initialize(array $options) 
  {
    $this->addBehavior( 'I18n.I18nable', [
      'fields' => ['title']
    ]);
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Slug.Sluggable');
    
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Slug\Test\TestCase\Model\Behavior\Content');
  }
}

class Content extends Entity 
{
  use TranslateTrait;
  use CrudEntityTrait;
  use SlugTrait;

  protected $_accessible = [
    'slugs' => true,
    'title' => true,
  ];
}


/**
 * Slug\Model\Behavior\SluggableBehavior Test Case
 */
class SluggableBehaviorTest extends TestCase
{

  public $fixtures = [
    'Contents' => 'plugin.slug.contents',
    'Languages' => 'plugin.i18n.languages',
    'I18n' => 'plugin.slug.i18n',
    'Slugs' => 'plugin.slug.slugs',
    'plugin.website.sites'
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    if( Plugin::loaded( 'Website'))
    {
      Plugin::unload( 'Website');
    }

    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']); 
    $this->setLanguages();
    
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]); 
    
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset($this->Sluggable);
    unset($this->Contents);

    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

/**
 * Comprueba la inserción de un contenido nuevo
 */
  public function testSaveNew()
  {
    I18n::locale( 'spa');
    
    for( $i = 0; $i < 15; $i++)
    {
      // Guardar
      $content = $this->Contents->getNewEntity([
        '_translations' => [
          'spa' => [
            'title' => 'Hola amigos'
          ],
          'eng' => [
            'title' => 'Hello friends'
          ]
        ]
      ]);

      $saved = $this->Contents->saveContent( $content);
      $content = $this->Contents->find( 'content')
        ->where(['Contents.id' => $content->id])
        ->first();

      // El slug en español
      $slug = 'hola-amigos';

      if( $i > 0)
      {
        $slug .= '-'. $i;
      }

      $this->assertEquals( $slug, $content->slugger( 'spa')->slug);
      // El slug en inglés
      $slug = 'hello-friends';

      if( $i > 0)
      {
        $slug .= '-'. $i;
      }

      $this->assertEquals( $slug, $content->slugger( 'eng')->slug);
    }
  }

/**
 * Comprueba la actualización de registros
 */
  public function testUpdate()
  {
    $content = $this->Contents->find( 'content')
      ->where(['Contents.id' => 1])
      ->first();

    $data = $this->Contents->find( 'array')
      ->where(['Contents.id' => 1])
      ->first();

    $data ['spa']['title'] = 'Adiós mundo';
    $data ['eng']['title'] = 'Bye world';
    $data = json_decode( json_encode( $data), true);

    $data ['slugs'][0]['slug'] = 'adios-mundo';
    $content = $this->Contents->patchContent( $content, $data); 
    $saved = $this->Contents->saveContent( $content);

    $content = $this->Contents->find( 'content')
      ->where(['Contents.id' => $content->id])
      ->first();

    $this->assertEquals( 'adios-mundo', $content->slugger( 'spa')->slug);
  }

/**
 * Comprueba que si un el campo de un idioma está vacio, se crea el slug a partir del campo principa
 */
  public function testNewWhenEmpty()
  {
    // Guardar
    $content = $this->Contents->getNewEntity([
      'spa' => [
        'title' => 'Hola amigos'
      ],
      'eng' => [
        'title' => ''
      ]
    ]);

    $saved = $this->Contents->saveContent( $content);
    $content = $this->Contents->find( 'content')
      ->where(['Contents.id' => $content->id])
      ->first();

    $this->assertEquals( 'hola-amigos', $content->slugger( 'spa')->slug);
    $this->assertEquals( 'hola-amigos', $content->slugger( 'eng')->slug);
  }

/**
 * Test para comprobar el funcionamiento del método _getSlug del Trait SlugTrait
 */
  public function testEntitySlugProperty()
  {
    I18n::locale( 'spa');
    $content = $this->Contents->find( 'content')
      ->where(['Contents.id' => 1])
      ->first();

    $this->assertEquals( 'hola-mundo', $content->slug);
  }

/**
 * Comprueba el correcto funcionamiento de SluggalbleBehavior::findBySlug
 */
  public function testFindBySlug()
  {
    I18n::locale( 'spa');

    $content = $this->Contents->find( 'slug', ['slug' => 'hola-mundo'])
      ->first();

    $this->assertEquals( 'hola-mundo', $content->slug);
  }


  public function testSaveSlugs()
  {
    I18n::locale( 'spa');
    $content = $this->Contents->getNewEntity([
      'spa' => [
        'title' => 'Hola amigos'
      ],
      'eng' => [
        'title' => ''
      ],
      'slugs' => [
        [
          'locale' => 'spa',
          'slug' => '',
          'model' => 'Contents'
        ],
        [
          'locale' => 'eng',
          'slug' => '',
          'model' => 'Contents'
        ]
      ]
    ]);

    $saved = $this->Contents->saveContent( $content);
    $content = $this->Contents->find( 'content')
      ->where(['Contents.id' => $content->id])
      ->first();

    $this->assertEquals( 2, count( $content->slugs));
    $this->assertEquals( 'hola-amigos', $content->slugs [0]->slug);

  }

  public function testNewEntity()
  {
    $content = $this->Contents->getNewEntity( []);
    $a = $this->Contents->emptyEntityProperties( $content);
  }
}
