<?php
namespace Slug\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Slug\Model\Table\SlugsTable;

/**
 * Slug\Model\Table\SlugsTable Test Case
 */
class SlugsTableTest extends TestCase
{

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
    'Slugs' => 'plugin.slug.slugs'
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      $config = TableRegistry::exists('Slugs') ? [] : ['className' => 'Slug\Model\Table\SlugsTable'];
      $this->Slugs = TableRegistry::get('Slugs', $config);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset($this->Slugs);

      parent::tearDown();
  }

  /**
   * Test initialize method
   *
   * @return void
   */
  public function testInitialize()
  {

  }

}
